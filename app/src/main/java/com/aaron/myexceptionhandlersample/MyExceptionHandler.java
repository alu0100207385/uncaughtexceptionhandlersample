package com.aaron.myexceptionhandlersample;

import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;

public class MyExceptionHandler implements Thread.UncaughtExceptionHandler {

    private final Context myContext;
    private final Class<?> myActivityClass;

    public MyExceptionHandler(Context context, Class<?> c) {
        myContext = context;
        myActivityClass = c;
    }

    public void uncaughtException(Thread thread, Throwable exception) {
        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        String s = stackTrace.toString();
        // You can use LogCat too
        //System.err.println(stackTrace);
        Log.e("Error", s);
        Intent intent = new Intent(myContext, myActivityClass);
        intent.putExtra("uncaughtException", "Exception is: " + s);
        intent.putExtra("stacktrace", s);
        myContext.startActivity(intent);
        //for restarting the Activity
        Process.killProcess(Process.myPid());
        System.exit(0);
    }
}
