package com.aaron.myexceptionhandlersample;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermission();

        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, MainActivity.class));

        String exception = getIntent().getStringExtra("uncaughtException");
        if (exception != null){
            String s = getIntent().getStringExtra("stacktrace");
            Log.e("Trace", s);
            final AlertDialog.Builder builder = customDialog(s);
            alertDialog = builder.create();
            alertDialog.show();
        }
        initializes();
    }


    private void initializes(){
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = getIntent().getStringExtra("boom");
                if (a.equals("bla")) {
                    ((TextView) findViewById(R.id.result)).setText(a);
                }
            }
        });
    }

    private void requestPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 80);
        }
    }


    private AlertDialog.Builder customDialog(final String msg){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("Error");
        builder.setMessage(msg);
        builder.setNeutralButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        builder.setNegativeButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                saveToFile(msg);
            }
        });
        builder.setPositiveButton("Report", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] { msg });
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Error report");
                startActivity(Intent.createChooser(emailIntent, "Sending report ..."));
            }
        });
        return builder;
    }


    private void saveToFile(String msg){
        try {
            // Creates a file in the primary external storage space of the
            // current application.
            // If the file does not exists, it is created.
            //File testFile = new File(this.getExternalFilesDir(null), "report.txt");
            File testFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "report.txt");
            Log.d("file", testFile.getAbsolutePath());
            if (!testFile.exists())
                testFile.createNewFile();

            // Adds a line to the file
            BufferedWriter writer = new BufferedWriter(new FileWriter(testFile, true /*append*/));
            writer.write(msg);
            writer.close();
            Toast.makeText(this, "Saved in report.txt", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Log.e("ReadWriteFile", "Unable to write: " + e.getMessage());
            Toast.makeText(this, "Unable to write: " + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}
